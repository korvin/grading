CURRENT_PATH=$(pwd)
NEW_GOPATH="$CURRENT_PATH"
if [ -d "$CURRENT_PATH/vendor" ]; then
	NEW_GOPATH="$CURRENT_PATH/vendor:$CURRENT_PATH"
fi

export GOPATH="$NEW_GOPATH"

echo "GOPATH set to $GOPATH"




